## Question 1 : Lancer un container nginx et valider avec un navigateur que l'on peut bien l'afficher

*Etape 1 :*
- Se rendre sur l'url : https://hub.docker.com/_/nginx

*Etape 2 :*
- Pull l'image docker de nginx 
- commande : docker pull nginx

*Etape 3 :*
- Créer un container et le run
- comande : docker run --name docker-nginx -p 80:80 nginx

*Etape 4 :*
- Ouvrir son navigateur sur son localhost:80
