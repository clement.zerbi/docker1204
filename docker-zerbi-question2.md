## Question 2 : Lancer un container MariaDB 10.0.1

*Etape 1 :*
- Se rendre sur l'url : https://hub.docker.com/_/mariadb


*Etape 2 :*
On crée un volume qui servira de base de donnée commune entre nos 2 containers.
- docker volume create db

*Etape 3 :*
- Créer un container et le run
- comande : docker run --name newdb --volume db -e MYSQL_ROOT_PASSWORD=root -d mariadb:10.1 

*Etape 4 :*
On éxecute le container en bash afin de pouvoir se connecter en sql
- docker exec -it newdb bash

*Etape 5 :*
On se connecte en sql
- mysql -u root -p
password : root


*Etape 6:*
On crée une base de donnée et on y ajoute une table + une valeur


- CREATE DATABASE eemi;

- USE eemi;

- CREATE TABLE ELEVES(ID INT NOT NULL, NAME VARCHAR (20), PRIMARY KEY (ID));

- INSERT INTO ELEVES (NAME) VALUE ('Zerbi');

- SELECT * FROM ELEVES; (on vérifie que cela a fonctionné).

- exit (afin de quitter le container)

*Etape 7 :*

On crée un 2ème container en 10.3 qui lui aussi utilise le volume db afin d'avoir les mêmes données

- docker run --name newdb2 --volume db -e MYSQL_ROOT_PASSWORD=root -d mariadb:10.3




